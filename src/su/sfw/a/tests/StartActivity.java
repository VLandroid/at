package su.sfw.a.tests;

import su.sfw.a.todolist.ToDoList;
import su.sfw.a.xmlparser.XMLEntryPoint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
//import su.sfw.a.debug.*;


public class StartActivity extends Activity {

	static final private int TODO = Menu.FIRST;
	static final private int PARSE = Menu.FIRST+1;
	static final private int GEO = Menu.FIRST+2;
	static final private int SETTINGS = Menu.FIRST+3;
	static final private int VER = Menu.FIRST+4;
	static final private int EXIT = Menu.FIRST+5;
		
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_screen);
        
        //change text at main screen        
        setMainText();
        //hello Toast
        useHelloToast();
    }
    @Override
    public void onResume(){
    	super.onResume();
    	setMainText();
        //hello Toast
        useHelloToast();
    }
    
    private void setMainText(){
    	
        //get TextView
        TextView t = (TextView)findViewById(R.id.mainText);
        //get pref
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String mainScreenText = prefs.getString("PREF_MAIN_TEXT","");
       
        //check on emptyness
        if(mainScreenText.equals("")){
       	 //set default
       	 t.setText(R.string.ns_text);
        } else {
       	 //set custom
       	 t.setText(mainScreenText);
        }
    }
    private void useHelloToast(){
    	//getting settings
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
    	boolean useHT = prefs.getBoolean("PREF_USE_HT",true);
        String HTText = prefs.getString("PREF_HT_TEXT",getString(R.string.ht_text_default));
        String HTDuration = prefs.getString("PREF_HT_DURATION","0");
        //logic
        if(useHT){
        	//show toast
        	Context context = getApplicationContext();
        	int duration = Toast.LENGTH_SHORT;
        	if(HTDuration.equals("1")){
        		duration = Toast.LENGTH_LONG;
        	}
        	Toast.makeText(context,HTText, duration).show();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	//add items
    	MenuItem itemToDo = menu.add(0,TODO,Menu.NONE,R.string.ns_m_todo);
    	MenuItem itemParse = menu.add(0,PARSE,Menu.NONE,R.string.ns_m_parser);
    	MenuItem itemGeo = menu.add(0,GEO,Menu.NONE,R.string.ns_m_geo);
    	MenuItem itemSettings = menu.add(0,SETTINGS,Menu.NONE,R.string.ns_m_settings);
    	MenuItem itemVer = menu.add(0,VER,Menu.NONE,R.string.ns_m_version);
    	MenuItem itemExit = menu.add(0,EXIT,Menu.NONE,R.string.ns_m_exit);
    	
    	//add ShortCuts
    	itemToDo.setShortcut('1','t');
    	itemParse.setShortcut('2','p');
    	itemGeo.setShortcut('3','g');
    	itemSettings.setShortcut('4','s');
    	itemVer.setShortcut('5', 'v');
    	itemExit.setShortcut('6','e');
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	//on menu items selected
    	super.onOptionsItemSelected(item);
    	switch(item.getItemId()){
    	case TODO:
    		this.moveToTODO();
    		return true;	
    	case PARSE:
    		this.moveToParser();
    		break;
    	case GEO:
    		this.notReadyToast();
    		break;
    	case SETTINGS:
    		this.moveToSettings();
    		break;
    	case VER:
    		this.showVersion();
    		break;
    	case EXIT:
    		this.exit();
    		break;
    	}
    	
    	return true;
    }
    
    //Items reactions
    private void moveToTODO(){
    	Intent intent = new Intent(StartActivity.this, ToDoList.class);
    	startActivity(intent);
    }
    
    private void moveToParser(){
    	Intent intent = new Intent(StartActivity.this, XMLEntryPoint.class);
    	startActivity(intent);
    }
    
    private void moveToSettings(){
    	Intent intent = new Intent(StartActivity.this, MainSettings.class);
    	startActivity(intent);
    }
    
    private void notReadyToast(){
    	//show toast
    	int duration = Toast.LENGTH_SHORT;
    	Context context = getApplicationContext();
    	Toast.makeText(context,R.string.not_ready_toast, duration).show();
    	
    }
    private void showVersion(){
    	String versionName="";
    	try {
			versionName = this.getPackageManager().getPackageInfo(this.getPackageName(), 0 ).versionName;
		} catch (NameNotFoundException e) {
			versionName="Undefined";
		}
    	//show toast
    	int duration = Toast.LENGTH_SHORT;
    	Context context = getApplicationContext();
    	Toast.makeText(context,versionName, duration).show();
    }
    private void exit(){
    	
    	//show toast
    	Context context = getApplicationContext();
    	int duration = Toast.LENGTH_LONG;
    	Toast.makeText(context,R.string.thanx_toast, duration).show();
    	//go to home screen
    	//and android OS, will kill all activities after
    	Intent backToMain = new Intent(Intent.ACTION_MAIN);
    	backToMain.addCategory(Intent.CATEGORY_HOME);
    	backToMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	startActivity(backToMain);
    }
   
}

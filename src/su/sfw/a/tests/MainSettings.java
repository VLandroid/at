package su.sfw.a.tests;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class MainSettings extends PreferenceActivity {
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.main_settings);
	}

}
//TODO 1) Юзать helloToast, тип checkbox, default - no
//	   2) Сообщение, тип EditText, default - "Change me at settings"


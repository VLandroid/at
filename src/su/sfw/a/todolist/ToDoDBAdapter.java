package su.sfw.a.todolist;

import java.sql.SQLException;
import java.util.Date;

import su.sfw.a.debug.Debug;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;



@SuppressWarnings("unused")
public class ToDoDBAdapter {
	private static final String DB_NAME="todoDB";
	private static final String DB_TABLE="todoTable";
	private static final int DB_VERSION=1;
	
	
	
	private SQLiteDatabase db;
	private final Context context;
	
	private ToDoDBOpenHelper dbHelper;
	
	public static final String KEY_ID = "_id";
	public static final String KEY_TASK = "task";
	public static final String KEY_CREATION_DATE="creation_date";
	
	public static final int TASK_COL = 1; 
	public static final int CREATION_DATE_COL = 2;  
	
	public ToDoDBAdapter(Context _context){
		this.context = _context;
		this.dbHelper = new ToDoDBOpenHelper(_context, DB_NAME, null, DB_VERSION);
	}
	
	public void open(){
		try{
			db = this.dbHelper.getWritableDatabase();
		}catch(SQLiteException e){
			db = this.dbHelper.getReadableDatabase();
		}
	}
	
	public void close(){
		db.close();
	}
	
	//CRUD
	//ins
	public long insertTask(ToDoItem task){
		//create new values storage (container)
		ContentValues newTaskValues = new ContentValues();
		//Assign values for each line
		newTaskValues.put(KEY_TASK,task.getTask());
		newTaskValues.put(KEY_CREATION_DATE,task.getCreated().getTime());
		//do insert
		return db.insert(DB_TABLE, null, newTaskValues);
	}
	//del
	public boolean deleteTask(long rowIndex){
		return db.delete(DB_TABLE, KEY_ID + "=" + rowIndex, null) > 0;
	}
	public boolean deleteTask(String recordText){
		return db.delete(DB_TABLE, KEY_TASK + "=" + "\"" +recordText +"\"", null) > 0;
	}
	//upd
	public boolean updateTask(long rowIndex, String task){
		ContentValues values = new ContentValues();
		values.put(KEY_TASK, task);
		return db.update(DB_TABLE, values, KEY_ID + "="+ rowIndex, null) >0;
	}
	public boolean updateTask(String curText,String newText){
		ContentValues values = new ContentValues();
		values.put(KEY_TASK, newText);
		return db.update(DB_TABLE, values, KEY_TASK + "=" + "\"" + curText + "\"" ,null)>0;
	}
	//Selects
	//cursor for all items
	public Cursor getAllItemsCursor(){
		return db.query(DB_TABLE,
						new String[] { KEY_ID, KEY_TASK, KEY_CREATION_DATE}, 
						null, null, null, null, null);
	}
	//cursor for one item
	public Cursor setCursorToItem(long rowIndex) throws SQLException{
		Cursor rslt = db.query(DB_TABLE, 
								new String[]{KEY_ID, KEY_TASK}, 
								KEY_ID + "=" + rowIndex,
								null, null, null, null,null);
		if(rslt.getCount() == 0 || !rslt.moveToFirst()){
			//TODO remove hardcoded String
			throw new SQLException("No items found for row: "+ rowIndex);
		}
		return rslt;
	}
	//select single item
	public ToDoItem getItem(long rowIndex) throws SQLException{
		Cursor cursor = db.query(true, DB_TABLE, 
								new String[]{ KEY_ID, KEY_TASK}, 
								KEY_ID + "=" + rowIndex, 
								null, null, null, null, null);
		if(cursor.getCount() == 0 || !cursor.moveToFirst()){
			throw new SQLException("No item found for row: " + rowIndex);
		}
		
		String task = cursor.getString(TASK_COL);
		long created = cursor.getLong(CREATION_DATE_COL);
		
		ToDoItem result = new ToDoItem(task, new Date(created));
		return result;
	}
	public boolean getItemByText(String text){
		Cursor cursor = db.query(true, DB_TABLE,
								new String[]{KEY_TASK},
								KEY_TASK + " = " + "\"" + text + "\"", 
								null , null, null, null, null);
		if(cursor.getCount()==1){
			return true;
		} else {
			return false;
		}
	}
	
	//HELPER CLASS
	private static class ToDoDBOpenHelper extends SQLiteOpenHelper{
		//Query for creating new DB 
		private static final String DB_CREATE = "create table "+ 
				DB_TABLE + " (" + KEY_ID + " integer primary key autoincrement, "+
				KEY_TASK + " text not null, " + KEY_CREATION_DATE + " long);";
		
		public ToDoDBOpenHelper(Context context, String name, CursorFactory factory, int version){
			super(context, name, factory, version);
		}
		
		
		
		@Override
		public void onCreate(SQLiteDatabase _db) {
			_db.execSQL(DB_CREATE);
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
			//TODO log update progress (can be done with Debug lib)
			
			//TODO make normally (backup->destroy->re-create)
			//Destroy old table
			_db.execSQL("DROP TABLE IF EXISTS "+ DB_TABLE);
			//Create new
			onCreate(_db);
		}
		
	}
}

package su.sfw.a.todolist;

import java.util.ArrayList;
import java.util.Date;

import su.sfw.a.debug.Debug;
import su.sfw.a.tests.R;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnKeyListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//TODO Do delete Tasks method via longPressListener + Context Menu 

@SuppressWarnings("unused")
public class ToDoList extends Activity {
	//for menu
	static final private int ADD_NEW_TODO = Menu.FIRST;
	static final private int REMOVE_TODO = Menu.FIRST + 1;
	static final private int ADD_SCREEN = Menu.FIRST + 2;
	private boolean addNew = false;
	
	private ListView myListView;
	private EditText myEditText;
	private ArrayList<ToDoItem> toDoItems;
	private ArrayAdapter<ToDoItem> aa;
	private ToDoDBAdapter dbAdapter;
	private Cursor cursor;
	private Button addBtn;
	
	private View dialogView;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //load layout
        setContentView(R.layout.main);
        //get links to elements
       this.myListView =(ListView)findViewById(R.id.myListView);
       this.myEditText = (EditText)findViewById(R.id.myEditText);
       this.addBtn = (Button)findViewById(R.id.addBtn); 
       
        //Store Items to ArrayList
        this.toDoItems = new ArrayList<ToDoItem>();
        //Array Adapter
        this.aa = new ArrayAdapter<ToDoItem>(this,android.R.layout.simple_list_item_1,toDoItems);
        //Link ArrayList to Adapter
        myListView.setAdapter(aa);
        
        addBtn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				//adding Item
				addNewItem();
			}
		});
        
        //Register Context Menu for Current View
        registerForContextMenu(myListView);
        
        //Use DB adapter
        this.dbAdapter = new ToDoDBAdapter(this);
        //TODO Fix Error. Error is here
        this.dbAdapter.open();
       
       populateList();
        
    }//end of onCreate
    
    private void populateList(){
    	//get all elements from DB
    	this.cursor = dbAdapter.getAllItemsCursor();
    	//check for many items to we have
    	if(cursor.moveToFirst()){
    		startManagingCursor(cursor);
        	//update Array
        	updateArray();
    	} else {
    		//no records
    		Context context = getApplicationContext();
			int duration = Toast.LENGTH_LONG;
			Toast noRecToast = Toast.makeText(context,R.string.no_records_yet, duration);
			noRecToast.setGravity(Gravity.TOP|Gravity.CENTER,0,200);
			noRecToast.show();
    	}
    	
    }
    
    private void updateArray(){
    	this.cursor.requery();
    	
    	toDoItems.clear();
    	
    	if(this.cursor.moveToFirst()){
    		do {
    			String task = this.cursor.getString(ToDoDBAdapter.TASK_COL);
    			long created = this.cursor.getLong(ToDoDBAdapter.CREATION_DATE_COL);
    			
    			ToDoItem newItem = new ToDoItem(task,new Date(created));
    			toDoItems.add(newItem);
    			
    		}while(this.cursor.moveToNext());
    			aa.notifyDataSetChanged();
    	}
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo){
    	super.onCreateContextMenu(menu, v, menuInfo);
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.item_menu, menu);	
    }
    @Override
    public boolean onContextItemSelected(MenuItem item){
    	AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
    	switch(item.getItemId()){
    	case R.id.item_menu_edit:
    		//Get current text
    		TextView tv = (TextView) info.targetView;
    		final String curText = (String) tv.getText();
    		//init View for result
        	LayoutInflater li = LayoutInflater.from(this);
        	this.dialogView = li.inflate(R.layout.edit_item_dialog,null);
        	final EditText newTextInput = (EditText) dialogView.findViewById(R.id.newTextInput);
    		//Make dialog
    		AlertDialog.Builder d = new AlertDialog.Builder(this);
        	newTextInput.setText(curText);
        	//create dialog
        	d.setTitle("Edit Item")
        	.setView(dialogView)
        	.setCancelable(false)
        	.setPositiveButton("OK", new OnClickListener(){
        		public void onClick(DialogInterface dialog, int id){
        			String newText = newTextInput.getText().toString();
        			//check if newText is not empty
        			if(newText.length()==0){
        				//show toast
        				Context context = getApplicationContext();
        				int duration = Toast.LENGTH_SHORT;
        				Toast.makeText(context,R.string.empty_record_toast, duration).show();
        			} else {
        				editItem(curText, newText);
        			}	
        		}
        	});
        	
        	d.create().show();
    		
    		//send to update
    		//
    		return true;
    	case R.id.item_menu_delete:
    		tv = (TextView) info.targetView;
    		String recordText = (String) tv.getText();
    		removeItem(recordText);
    		return true;
    	default:
    		return false;
    	}
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	super.onCreateOptionsMenu(menu);
    	
    	//creating menu items
    	MenuItem itemAdd = menu.add(0,ADD_NEW_TODO,Menu.NONE,R.string.add_new);
    	MenuItem itemRem = menu.add(0, REMOVE_TODO, Menu.NONE,R.string.remove);
    	MenuItem itemAddSc = menu.add(0,ADD_SCREEN, Menu.NONE,R.string.test);
    	//Setting icons
    	itemAdd.setIcon(R.drawable.add);
    	itemRem.setIcon(R.drawable.delete);
    	
    	//keyboard shortcuts
    	itemAdd.setShortcut('0','a');
    	itemRem.setShortcut('1','r');
    	itemAddSc.setShortcut('2','t');
    	
    	return true;
    	
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
    	super.onPrepareOptionsMenu(menu);
    	
    	int idx = myListView.getSelectedItemPosition();
    	
    	String removeTitle = getString(addNew ? R.string.cancel : R.string.remove);
    	
    	MenuItem removeItem = menu.findItem(REMOVE_TODO);
    	removeItem.setTitle(removeTitle);
    	removeItem.setVisible(addNew || idx > -1);
    	
    	return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	super.onOptionsItemSelected(item);
    	
    	int index = myListView.getSelectedItemPosition();
    	
    	switch(item.getItemId()){
    	
    	case (REMOVE_TODO):
    		if(addNew){
    			cancelAdd();
    		} else {
    			removeItem(index);
    		}
    		return true;
    	case (ADD_NEW_TODO):
    		newItem();
    		addNewItem();
    		return true;
    	case (ADD_SCREEN):
    		this.moveToStart();
    		return true;
    	}
    	return false;
    }
    
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	//and close DB Connection
    	this.dbAdapter.close();
    }
    private void addNewItem(){
    	String text = myEditText.getText().toString();
		if(text.length()==0){
			//show toast
			Context context = getApplicationContext();
			int duration = Toast.LENGTH_SHORT;
			Toast.makeText(context,R.string.empty_record_toast, duration).show();
		} else {
			//else search if we already have that record
			if(dbAdapter.getItemByText(text)){
				//found -show toast
				Context context = getApplicationContext();
				int duration = Toast.LENGTH_LONG;
				Toast.makeText(context,R.string.record_exists_toast, duration).show();
			} else {
			//not found - add
			ToDoItem newItem = new ToDoItem(text);
			dbAdapter.insertTask(newItem);
			this.updateArray();
			aa.notifyDataSetChanged();
			cancelAdd();
			myEditText.setText("");
			}
		}
    }
    private void newItem(){
    	addNew = true;
    	myEditText.setVisibility(View.VISIBLE);
    	myEditText.requestFocus();
    }
    private void cancelAdd(){
    	addNew = false;
    	myEditText.setVisibility(View.VISIBLE);
    }
    private void editItem(String itemText,String newText){
    	dbAdapter.updateTask(itemText, newText);
    	this.updateArray();
    	aa.notifyDataSetChanged();
    }
    private void removeItem(long _index){
    	dbAdapter.deleteTask(_index);
    	this.updateArray();
    	aa.notifyDataSetChanged();
    }
    private void removeItem(String recordText){
    	dbAdapter.deleteTask(recordText);
    	this.updateArray();
    	aa.notifyDataSetChanged();
    }
    private void moveToStart(){
    	finish();
    }
}

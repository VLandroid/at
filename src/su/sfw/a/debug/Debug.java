package su.sfw.a.debug;

import android.util.Log;

public class Debug {
	
	public static void eLog(String var){
			eLog("DEBUG", var);
	}
	
	public static void eLog(String label, String var){
		Log.e(label, var);
	}
	
	public static String toString(int i){
		return ""+ i ;
	}
	public static String toString(long i){
		return ""+ i ;
	}
	public static String toString(boolean b){
		String bStr;
		if(b){
			bStr = "true";
		} else {
			bStr = "false";
		}
		return bStr;
	}

}

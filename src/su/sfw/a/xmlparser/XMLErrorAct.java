package su.sfw.a.xmlparser;

import su.sfw.a.tests.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

public class XMLErrorAct extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get info from intent
        Intent i = getIntent();
        
        String mess = i.getStringExtra("mess");
        int type = i.getIntExtra("type", 0);
        
        //Routing
        switch (type) {
		case XMLMagic.FATAL:
			fatalDialog(mess);
			break;
		case XMLMagic.MALFORMED:
			malformedDataDialog(mess);
			break;
		case XMLMagic.RECOVERABLE:
			tryAgainDialog(mess);
			break;
		default:
			break;
		}
    }//end of onCreate
    
    public void onRestart(){
    	super.onRestart();
    	Intent intent = new Intent(XMLErrorAct.this,StartActivity.class);
    	startActivity(intent);
    }
    //Dialogs
    private void fatalDialog(String mess){
    	//create new alert Dialog
        AlertDialog.Builder d = new AlertDialog.Builder(this); 
        //title
        d.setTitle(R.string.xml_e_title);
        //text
        d.setMessage(mess)
        //cancel
        .setCancelable(false)
        //OK button
        .setPositiveButton(R.string.xml_e_ok,
        	new OnClickListener() {
					
			public void onClick(DialogInterface dialog, int which) {
			goStart();
				}
        	});
        //create and show
        d.create().show();
    }
    private void tryAgainDialog(String mess){
    	 //create new alert Dialog
        AlertDialog.Builder d = new AlertDialog.Builder(this); 
        //title
        d.setTitle(R.string.xml_e_title);
        //text
        d.setMessage(mess)
        //cancel
        .setCancelable(false)
        //positive button
        .setPositiveButton(R.string.xml_e_tryagain,
        	new OnClickListener() {
					
			public void onClick(DialogInterface dialog, int which) {
			goFwd();
				}
        	})
        .setNegativeButton(R.string.xml_e_back, 
        		new OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
			goPrev();	
			}
		});
        //create and show
        d.create().show();
    }
    
    private void malformedDataDialog(String mess){
    	 //create new alert Dialog
        AlertDialog.Builder d = new AlertDialog.Builder(this); 
        //title
        d.setTitle(R.string.xml_e_title);
        //text
        d.setMessage(mess)
        //cancel
        .setCancelable(false)
        //positive button
        .setPositiveButton(R.string.xml_e_ok,
        	new OnClickListener() {
					
			public void onClick(DialogInterface dialog, int which) {
			goFwd();
				}
        	});
        //create and show
        d.create().show();
    }
    
    //Actions
    private void goStart(){
    	Intent intent = new Intent(XMLErrorAct.this,StartActivity.class);
    	startActivity(intent);
    }
    
    private void goFwd(){
    	Intent intent = new Intent(XMLErrorAct.this,XMLMain.class);
    	startActivity(intent);
    }
    
    private void goPrev(){
    	Intent intent = new Intent(XMLErrorAct.this,StartActivity.class);
    	startActivity(intent);
    }
	    
}

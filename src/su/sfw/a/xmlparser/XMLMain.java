package su.sfw.a.xmlparser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import su.sfw.a.debug.Debug;
import su.sfw.a.tests.*;
import su.sfw.java.xml.*;

import android.os.Bundle;

import android.preference.PreferenceManager;

import android.annotation.SuppressLint;
import android.app.ListActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

@SuppressWarnings("unused")
public class XMLMain extends ListActivity {
	
	static final private int BACK = Menu.FIRST;
	static final private int SETTINGS = Menu.FIRST+1;
	
	static final private String FILENAME = "tmpXML";
	//final private 
	
	ArrayList<HashMap<String, String>> Wizards = new ArrayList<HashMap<String,String>>();
	WizXMLparser wP = new WizXMLparser();
	
	//flags (from intent)
	boolean fileFlag;
	
	//settings
	int numOfRec;
	boolean useTmpFile;
	String tmpFileName;
	
	private void read(){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		//num of rec
		String numRec= prefs.getString("PREF_NUM_REC","4");
    	this.numOfRec = Integer.parseInt(numRec);
    	//tmp file
    	this.useTmpFile = prefs.getBoolean("PREF_TMP_FILE",true);
    	//tmp file name
    	this.tmpFileName = prefs.getString("PREF_TMP_FILE_NAME","void");
	}
	private void cleaner(){
		if(this.useTmpFile == false){
    		deleteFile(FILENAME);
    	}
	}
	
	@SuppressLint("CommitPrefEdits")
	private SharedPreferences.Editor getSettingsEditor(){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		return editor;
	}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_main);
       
    	//read settings
    	read();	
    	//Clear Current list
    	this.Wizards.clear();
        //parse
        parse();
        //show
        show(); 
    }
    
    public void onRestart(){
    	super.onRestart();
    	//read settings
    	read();
    	//run cleaner
    	cleaner();
    	//Clear Current list
    	this.Wizards.clear();
    	//Re-parse
    	parse();
    	//and re-draw
    	show();
    }
    
    private void parse(){
    	//prepare
    	//create parser and set url
        
    	 //check on fileFlag
        this.fileFlag = getIntent().getBooleanExtra("fileFlag",false);
        if(this.fileFlag){
        	//FILE->ARRAY
        	this.parseFromFile();
        } else {
        	//let's look we are permitted to use tmp file
        	if(this.useTmpFile){
        		//we are
        		//NET->FILE->ARRAY
        		this.update();
        		this.parseFromFile();
        	} else {
        		//NET->ARRAY
        		//we aren't
        		this.net2Array();
        	}
        }   	 
    }
    private void parseFromFile(){
    	//init 
    	FileInputStream file = null;
    	//get file
    	try {
			file = openFileInput(this.tmpFileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//parse from file
    	try {
			wP.parseFeed(this.Wizards,this.numOfRec,file);
		} catch (SAXException e) {
			String mess = getString(R.string.xml_em_parse);
			gotoError(XMLMagic.FATAL, mess);
		} catch (IOException e) {
			//TODO Fix it (instead of this IO Throws)
			String mess = getString(R.string.xml_em_conn);
			gotoError(XMLMagic.RECOVERABLE, mess);
		} catch (ParserConfigurationException e) {
			String mess = getString(R.string.xml_em_parse);
			gotoError(XMLMagic.FATAL, mess);
		}
    }
    private void update(){
    	 //create File (to write to)
		FileOutputStream xmlFileWrite=null;
		try {
			xmlFileWrite = openFileOutput(XMLMain.FILENAME,Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String feedSource = getString(R.string.xml_feed);
		 //parse From Net to File
		 try {
			WizXMLparser.updateXML(feedSource, xmlFileWrite);
		} catch (ConnectException e) {
			//TODO Fix it (instead of this IO Throws)
			String mess = getString(R.string.xml_em_conn);
			gotoError(XMLMagic.RECOVERABLE, mess);
		} catch (MalformedURLException e) {
			String mess = getString(R.string.xml_em_mal);
			gotoError(XMLMagic.MALFORMED, mess);
		} catch (IOException e) {
			//TODO Fix it (instead of this IO Throws)
			String mess = getString(R.string.xml_em_conn);
			gotoError(XMLMagic.RECOVERABLE, mess);
		}
		 //setFileName setting
		 SharedPreferences.Editor editor = getSettingsEditor();
		 editor.putString("PREF_TMP_FILE_NAME",XMLMain.FILENAME);
		 editor.commit();
		 this.tmpFileName=XMLMain.FILENAME;
    }
    private void net2Array(){
    	//use net parser
    	String feedSource = getString(R.string.xml_feed);
        try {
			this.Wizards = wP.parseFeed(Wizards,this.numOfRec,feedSource);
		} catch (ConnectException e) {
			//TODO Fix it (instead of this IO Throws)
			String mess = getString(R.string.xml_em_conn);
			gotoError(XMLMagic.RECOVERABLE, mess);
		} catch (MalformedURLException e) {
			String mess = getString(R.string.xml_em_mal);
			gotoError(XMLMagic.MALFORMED, mess);
		} catch (IOException e) {
			String mess = getString(R.string.xml_em_conn);
			gotoError(XMLMagic.RECOVERABLE, mess);
		} catch (ParserConfigurationException e) {
			String mess = getString(R.string.xml_em_parse);
			gotoError(XMLMagic.FATAL, mess);
		} catch (SAXException e) {
			String mess = getString(R.string.xml_em_parse);
			gotoError(XMLMagic.FATAL, mess);
		}
    }
    
    
    private void show(){
    	//make adapter
        ListAdapter adapter = new SimpleAdapter(
        		this, 
        		Wizards, 
        		R.layout.xml_items,
        		new String[] {wP.NUM_NODE,wP.NAME_NODE,wP.POS_NODE,wP.COUNTRY_NODE},
        		new int[] {R.id.num,R.id.name,R.id.pos,R.id.country}
        );
        
        //use Adapter
        setListAdapter(adapter);
        
        //select single View
        ListView lv = getListView();
        lv.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id){
        		//getting values from selected single Item
        		String name = ((TextView) view.findViewById(R.id.name)).getText().toString();
        		
        		Intent i = new Intent(getApplicationContext(), XMLSingleItem.class);
        		i.putExtra("name",name);
        		startActivity(i);
        	}
        	
		});
    }
    
    //ERROR Handle
    private void gotoError(int level, String mess){
    	Intent i = new Intent(getApplicationContext(), XMLErrorAct.class);
		i.putExtra("type", level);
    	i.putExtra("mess",mess);
		startActivity(i);
    }
    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	//add items
    	MenuItem itemBack = menu.add(0,BACK,Menu.NONE,R.string.xml_m_back);
    	MenuItem itemSettings = menu.add(0,SETTINGS,Menu.NONE,R.string.xml_m_set);
    	
    	//add ShortCuts
    	itemBack.setShortcut('1','b');
    	itemSettings.setShortcut('2','s');
    	
    	return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	//on menu items selected
    	super.onOptionsItemSelected(item);
    	
    	switch(item.getItemId()){
    	case BACK:
    		moveBack();
    		return true;
    	case SETTINGS:
    		showSettings();
    		break;
    	}
		
		return false;
    }
    
    //menu Items reactions
	
	public void moveBack(){
		Intent i = new Intent(XMLMain.this,StartActivity.class);
		startActivity(i);
		finish();
	}
	
	public void showSettings(){
		Intent i = new Intent(XMLMain.this,XMLSettings.class);
		startActivity(i);
	}
   
}//end of class

final class WizXMLparser{
	//TODO store as file
	//define nodes (read-only)
	private final String ROOT_NODE = "player";
	public final String NAME_NODE = "name";
	public final String POS_NODE = "position";
	public final String NUM_NODE = "number";
	public final String COUNTRY_NODE = "country";
	
	private ArrayList<HashMap<String, String>> storageArray;
	private int records = 0;
	private NodeList nl = null;
	
	public static void updateXML(String feedSource,FileOutputStream dest) throws ConnectException, MalformedURLException, IOException{
		String xml = netXMLparser.XML2String(feedSource);
		dest.write(xml.getBytes());
		dest.close();
	}
	  /**
     * Parsing feed and adding HashMaps to ArrayList
     * 
     * @return void but modifies Wizards ListArray, by adding there HashMaps
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
     */	
	//overloading for file
	public ArrayList<HashMap<String, String>> parseFeed(ArrayList<HashMap<String, String>> storageArray, int records, FileInputStream feedSource) throws SAXException, IOException, ParserConfigurationException{
		this.storageArray = storageArray;
		this.records = records;
		
		//create parser block
		fileXMLparser parser = null;	
		parser = new fileXMLparser(feedSource);		
		
		//parse
		this.parse(parser);
		
		//return 
    	return this.storageArray;
	}
	//overload for net
    public ArrayList<HashMap<String, String>> parseFeed(ArrayList<HashMap<String, String>> storageArray, int records, String feedSource) throws ConnectException, MalformedURLException, IOException, ParserConfigurationException, SAXException{
    	this.storageArray = storageArray;
    	this.records = records;
    	
    	//create parser block
    	netXMLparser parser = null;
    	parser = new netXMLparser(feedSource);
    	
    	//parse
    	this.parse(parser);
    	
    	//return 
    	return this.storageArray;
    }//end of parseFeed
    
    private void parse(Parseable parser){
    	//get node list
    	if(parser != null){
    	nl = parser.getNodeList(ROOT_NODE);
    	}
    	
    	//how much record do we need? (0=all)
    	int numOfRec;
    	if(records == 0 ){
    		//all
    		numOfRec = nl.getLength();
    	} else if(records > nl.getLength()){
    		//if records more than we have (set to maximum items)
    		//to avoid outOfBound
    		numOfRec = nl.getLength();
    	} else {
    		//use records
    		numOfRec = records;
    	}

    	//loop thru all nodes
    	for (int i = 0; i < numOfRec;i++){
    		//create Hash Map for this item (player)
    		HashMap<String, String> map = new HashMap<String, String>();
    		//get element
    		Element e = (Element) nl.item(i);
    		//add to HashMap
    		map.put(NAME_NODE, parser.getValue(e, NAME_NODE));
    		map.put(POS_NODE,parser.getValue(e, POS_NODE));
    		map.put(NUM_NODE, parser.getValue(e, NUM_NODE));
    		map.put(COUNTRY_NODE, parser.getValue(e, COUNTRY_NODE));
    		
    		//add HashMap to ArrayList
    		this.storageArray.add(map);
    	}
    	    	
    }
}//end of wizParser class

package su.sfw.a.xmlparser;

import java.io.File;

import su.sfw.a.debug.Debug;
import su.sfw.a.tests.*;
import android.os.Bundle;
import android.preference.PreferenceManager;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.TextView;
import android.app.Activity;
import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;

@SuppressWarnings("unused")
public class XMLEntryPoint extends Activity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if(checkFile()){
        	//file dialog
        	commonDialog();
        } else {
        	//first run dialog
        	firstRunDialog();
        }
   
    }//end of onCreate
    
    private void firstRunDialog(){
    	//create new alert Dialog
        AlertDialog.Builder d = new AlertDialog.Builder(this); 
        //title
        d.setTitle(R.string.xml_frd_title)
        //text
        .setMessage(R.string.xml_frd_text)
        //cancel
        .setCancelable(false)
        //positive button
        .setPositiveButton(R.string.xml_frd_btn_yes,
        	new OnClickListener() {
					
			public void onClick(DialogInterface dialog, int which) {
			// TODO Move to next screen
			goFwd();
				}
        	})
        .setNegativeButton(R.string.xml_frd_btn_no, 
        		new OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
			// TODO Move to prev screen
			goPrev();	
			}
		});
        //create and show
        d.create().show();
    }
    
    private void commonDialog(){
    	//create new alert Dialog
        AlertDialog.Builder d = new AlertDialog.Builder(this); 
        //title
        d.setTitle(R.string.xml_d_title)
        //text
        .setMessage(R.string.xml_d_text)
        //cancel
        .setCancelable(false)
        //update button
        .setPositiveButton(R.string.xml_d_btn_upd,
        	new OnClickListener() {
					
			public void onClick(DialogInterface dialog, int which) {
			// TODO Move to next screen
			goFwd();
				}
        	})
        //use file button
        .setNeutralButton(R.string.xml_d_btn_file,
        	new OnClickListener() {
					
			public void onClick(DialogInterface dialog, int which) {
			// TODO Move to next screen
			goFwdWithFile();
				}
        	})	
        .setNegativeButton(R.string.xml_d_btn_back, 
        		new OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
			// TODO Move to prev screen
			goPrev();	
			}
		});
        //create and show
        d.create().show();
    }
    
    public void onRestart(){
    	super.onRestart();
    	Intent intent = new Intent(XMLEntryPoint.this,StartActivity.class);
    	startActivity(intent);
    }
    
    private void goFwd(){
    	Intent intent = new Intent(XMLEntryPoint.this,XMLMain.class);
    	intent.putExtra("fileFlag",false);
    	startActivity(intent);
    }
    private void goFwdWithFile(){
    	Intent intent = new Intent(XMLEntryPoint.this,XMLMain.class);
    	intent.putExtra("fileFlag",true);
    	startActivity(intent);
    }
    private void goPrev(){
    	finish();
    }
    
    private boolean checkFile(){
    	//reading prefs
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	String fName=prefs.getString("PREF_TMP_FILE_NAME","void");
    	if(fName.equals("void")){
    		//no file used
    		return false;
    	} else {
    		//check if file exists
    		File file = getFileStreamPath(fName);
    		if(file.exists()){
    			return true;
    		} else {
    			return false;
    		}
    	}
    }
}//end of Activity

/*
 *  //create Dialog
        Dialog d = new Dialog(XMLEntryPoint.this);
        
        //new Window
        Window window = d.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
        				WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        
        //set Title
        d.setTitle(R.string.xml_d_title);
        
        //link to View
        d.setContentView(R.layout.xml_entry_point);
        
        //set Text
        TextView dText = (TextView)d.findViewById(R.id.XMLDialogText);
        dText.setText(R.string.xml_d_text);
        
        //show
        d.show();
 */

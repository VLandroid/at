package su.sfw.a.xmlparser;

import su.sfw.a.tests.*;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class XMLSingleItem extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.single_item);
		
		TextView txtView = (TextView) findViewById(R.id.singleItemLabel);
		
		Intent i = getIntent();
		//getting Data from Intent
		String name = i.getStringExtra("name");
		txtView.setText(name);
	}
}

package su.sfw.a.xmlparser;

import su.sfw.a.tests.R;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class XMLSettings extends PreferenceActivity{
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
}

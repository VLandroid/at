package su.sfw.java.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class netXMLparser extends XMLparser {
	public netXMLparser(String xml) throws ConnectException,IOException,MalformedURLException, ParserConfigurationException, SAXException{
    	//get input stream
		InputStream in = getXML(xml);
    	//input stream to handle	
    	this.doc = this.InputHandler(in); 
    }
	
	public static void XML2File(String xml, File target) throws ConnectException, MalformedURLException, IOException{
		//got input stream
		InputStream in = getXML(xml);
		//write to file
		 //define file as Output Stream
		OutputStream dest = new FileOutputStream(target);
		//few more vars
		int read = 0;
		byte[] bytes = new byte[1024];
		
		while((read = in.read(bytes)) != -1 ){
			dest.write(bytes, 0, read);
		}
		
		in.close();
		dest.flush();
		dest.close();
		
	}
	public static String XML2String(String xml) throws ConnectException, MalformedURLException, IOException{
		//got input Stream
		InputStream in = getXML(xml);
		BufferedReader r = new BufferedReader(new InputStreamReader(in));
		StringBuilder total = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null){
			total.append(line);
		}
		//total is CharSequence
		//convert to String and return
		return total.toString();
	}
	
	private static InputStream getXML(String xml) throws ConnectException,IOException,MalformedURLException{
		URL url = null;
    	InputStream in=null;
    	URLConnection conn = null;
    	
				url = new URL(xml);
				
				try{
				conn = url.openConnection();
				}
				catch(IOException ioe){
					throw new ConnectException();
				}
    		
    		HttpURLConnection httpConn = (HttpURLConnection)conn;  		
    		int responseCode = 0;
				responseCode = httpConn.getResponseCode();

    		
    		if(responseCode == HttpURLConnection.HTTP_OK){
					in = httpConn.getInputStream();
    		}
    		return in;
    		
	}
}

package su.sfw.java.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class fileXMLparser extends XMLparser {
 	
	public fileXMLparser(String filename) throws SAXException, IOException, ParserConfigurationException{
 		this(new File(filename));
 	};
 	
	public fileXMLparser(File file) throws SAXException, IOException, ParserConfigurationException{
 		this.doc =  this.InputHandler(file);
 	};
	public fileXMLparser(FileInputStream fis) throws SAXException, IOException, ParserConfigurationException{
 		this.doc =  this.InputHandler(fis);
 	};

}

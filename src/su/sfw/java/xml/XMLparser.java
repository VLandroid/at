package su.sfw.java.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;


class XMLparser implements Parseable {
	Document doc;
		
		//PUBLIC IFACE
	
	 	public NodeList getNodeList(String rootElement){
	 		return this.doc.getElementsByTagName(rootElement);
	 	}
	 	
	 	public String getValue(Element item, String str) {
	 	   NodeList n = item.getElementsByTagName(str);
	 		return this.getElementValue(n.item(0));
	 	}
	 	
	 	//PROTECTED METHODS

		protected Document InputHandler(InputStream in) throws ParserConfigurationException, SAXException, IOException{
	 		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	 		DocumentBuilder db = dbf.newDocumentBuilder();
	 		
	 		Document dom = db.parse(in);
	 		return dom;
	 		
	 	}
		
		protected Document InputHandler(File file) throws SAXException, IOException, ParserConfigurationException{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	 		DocumentBuilder db = dbf.newDocumentBuilder();
	 		
	 		Document dom = db.parse(file);
	 		return dom;
		}
	 	
	 	private final String getElementValue( Node elem ) {
	         Node child;
	         if( elem != null){
	             if (elem.hasChildNodes()){
	                 for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
	                     if( child.getNodeType() == Node.TEXT_NODE  ){
	                         return child.getNodeValue();
	                     }
	                 }
	             }
	         }
	         return "";
	  } 
}

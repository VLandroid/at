package su.sfw.java.xml;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public interface Parseable {
	public NodeList getNodeList(String rootElement);
	public String getValue(Element item, String str);
}
